import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  // 路由模式
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'index',
      component: () => import('@/views/index.vue'),
      children: [
        {
          path: 'films',
          name: 'films',
          component: () => import('@/views/index/films.vue')
        },
        {
          path: 'active',
          name: 'active',
          component: () => import('@/views/index/active.vue')
        },
        {
          path: 'cinemas',
          name: 'cinemas',
          component: () => import('@/views/index/cinemas.vue')
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login.vue')
    },
    {
      path: '/user_oder',
      name: 'user_oder',
      component: () => import('@/views/user_oder.vue')
    },
    {
      path: '/film/:id',
      name: 'film',
      component: () => import('@/views/film.vue')
    },
    {
      path: '/user_red_packet',
      name: 'user_red_packet',
      component: () => import('@/views/user_red_packet.vue')
    }
  ]
})

export default router
