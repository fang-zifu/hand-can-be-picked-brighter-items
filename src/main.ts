import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import Persistedstate from 'pinia-plugin-persistedstate'

// 注册应用实例
const app = createApp(App)

// 创建pinia实例
const pinia = createPinia()

// 数据持久化
pinia.use(Persistedstate)
// 注册pinina
app.use(pinia)
app.use(router)

app.mount('#app')
